class Selectors {
    constructor(name) {
        this.elHP = document.getElementById(`health-${name}`);
        this.elProgressbar = document.getElementById(`progressbar-${name}`);
        this.elImg = document.getElementById(`img-${name}`);
        this.elName = document.getElementById(`name-${name}`);
    }
}

class Pokemon extends Selectors {
    constructor({ name, hp, type, selectors, attacks, img }) {
        super(selectors);
        this.name = name;
        this.hp = {
            current: hp,
            default: hp,
        };
        this.type = type;
        this.attacks = attacks;
        this.img = img; 
        this.renderHP();
    }

    renderHP() {
        this.elHP.innerText = this.hp.current + "/" + this.hp.default;
        this.elProgressbar.style.width = this.hp.current + '%';
    }

    renderPokemon() {
        this.elName.innerText = this.name; 
        this.elImg.src = this.img; 
        this.renderHP();
    }

    attack(count) {
        if (this.checkAlive(count) == true) {
            this.hp.current -= count;
        }
        else alert(this.name + ' проиграл бой!');
        this.renderHP();
    }

    checkAlive(count) {
        if (this.hp.current < count) {
            this.hp.current = 0;
            return false;
        }
        else {
            return true;
        }
    }

}

export default Pokemon;