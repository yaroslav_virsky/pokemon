const random = (num) => {
    return Math.ceil(Math.random() * num);
};
const countclicks = () => {
    let currentClicks = 0;
    return function (str, clicksLeft) {
        if (currentClicks < clicksLeft - 1) {
            currentClicks += 1;
            clicksLeft -= 1;
        }
        else {
            this.disabled = true;
        }
    }
};

const printlogs = (firstPerson, secondPerson, damageCount) => {
    const { name: FPname, hp: { default: FPdefaultHP, current: FPcurrentHP } } = firstPerson;
    const { name: SPname } = secondPerson;
    const logs = [
        `${FPname} вспомнил что-то важное, но неожиданно ${SPname}, не помня себя от испуга, ударил в предплечье врага. -${damageCount}, [${FPcurrentHP}/${FPdefaultHP}]`,
        `${FPname} поперхнулся, и за это ${SPname} с испугу приложил прямой удар коленом в лоб врага.-${damageCount}, [${FPcurrentHP}/${FPdefaultHP}]`,
        `${FPname} забылся, но в это время наглый ${SPname}, приняв волевое решение, неслышно подойдя сзади, ударил.-${damageCount}, [${FPcurrentHP}/${FPdefaultHP}]`,
        `${FPname} пришел в себя, но неожиданно ${SPname} случайно нанес мощнейший удар.-${damageCount}, [${FPcurrentHP}/${FPdefaultHP}]`,
        `${FPname} поперхнулся, но в это время ${SPname} нехотя раздробил кулаком \<вырезанно цензурой\> противника.-${damageCount}, [${FPcurrentHP}/${FPdefaultHP}]`,
        `${FPname} удивился, а ${SPname} пошатнувшись влепил подлый удар.-${damageCount}, [${FPcurrentHP}/${FPdefaultHP}]`,
        `${FPname} высморкался, но неожиданно ${SPname} провел дробящий удар.-${damageCount}, [${FPcurrentHP}/${FPdefaultHP}]`,
        `${FPname} пошатнулся, и внезапно наглый ${SPname} беспричинно ударил в ногу противника-${damageCount}, [${FPcurrentHP}/${FPdefaultHP}]`,
        `${FPname} расстроился, как вдруг, неожиданно ${SPname} случайно влепил стопой в живот соперника.-${damageCount}, [${FPcurrentHP}/${FPdefaultHP}]`,
        `${FPname} пытался что-то сказать, но вдруг, неожиданно ${SPname} со скуки, разбил бровь сопернику.-${damageCount}, [${FPcurrentHP}/${FPdefaultHP}]`
    ];
    const $p = document.createElement('p');
    $p.innerText = logs[random(logs.length) - 1];
    const $logs = document.querySelector('#logs');
    $logs.insertBefore($p, $logs.children[0]);
};

export default {random, countclicks, printlogs}
