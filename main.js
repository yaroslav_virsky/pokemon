import utils from "./utils.js"
import Game from "./game.js"

const $control = document.querySelector('.control');

let game = new Game;
gameStatus(); 

function gameStatus () {
    if (game.over) {
        game.restart();
        $control.innerHTML = ''; 
        drawButtons(); 
    }
    else {
        game.start();
        drawButtons(); 
    }
}


function drawButtons() {
    game.player1.attacks.forEach(item => {
        console.log(item);
        const $btn = document.createElement('button');
        $btn.classList.add('button');
        $btn.innerText = item.name;
        const btnCount = utils.countclicks();
        $btn.addEventListener('click', () => {
            let dmg = utils.random(item.minDamage, item.maxDamage);
            if (game.player2.hp.current > dmg) {
                game.player2.attack(dmg);
                console.log('click on button ', $btn.innerText);
                utils.printlogs(game.player2, game.player1, dmg);
                btnCount.call($btn, item.innerText, item.maxCount);
            }
            else {
                game.player2.attack(dmg);
                game.over = true; 
                console.log("gameover:" + game.over);
                gameStatus();
            }
        })
        $control.appendChild($btn);
    })
}

